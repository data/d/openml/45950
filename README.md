# OpenML dataset: Heart_Failure_Prediction

https://www.openml.org/d/45950

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
This dataset, named "heart_failure_clinical_records.csv", consists of clinical records of patients with heart failure. It includes various attributes such as age, anaemia, creatinine phosphokinase levels, diabetes status, ejection fraction, high blood pressure presence, platelet count, serum creatinine levels, serum sodium levels, gender, smoking habits, follow-up time, and a binary indicator for death event.

Attribute Description:
- age: age of the patient
- anaemia: presence of anaemia (0: no, 1: yes)
- creatinine_phosphokinase: level of creatinine phosphokinase in the blood
- diabetes: presence of diabetes (0: no, 1: yes)
- ejection_fraction: percentage of blood leaving the heart at each contraction
- high_blood_pressure: presence of high blood pressure (0: no, 1: yes)
- platelets: platelet count in the blood
- serum_creatinine: level of serum creatinine in the blood
- serum_sodium: level of serum sodium in the blood
- sex: gender of the patient (0: female, 1: male)
- smoking: smoking status of the patient (0: no, 1: yes)
- time: follow-up time
- DEATH_EVENT: indicator of death occurence during the follow-up period (0: no, 1: yes)

Use Case:
This dataset can be used for analyzing the relationship between various clinical attributes and the occurrence of death events in patients with heart failure. It can help in predicting the risk factors associated with heart failure mortality.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45950) of an [OpenML dataset](https://www.openml.org/d/45950). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45950/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45950/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45950/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

